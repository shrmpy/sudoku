package main

import (
	"flag"
	"fmt"
	"github.com/nsf/termbox-go"
	"github.com/shrmpy/sudoku"
	"math/rand"
	"strconv"
	"time"
	"unicode/utf8"
)

var debug = flag.Bool("debug", false, "Enable debug print")

func main() {
	err := termbox.Init()
	if err != nil {
		panic(err)
	}
	defer termbox.Close()

	termbox.SetInputMode(termbox.InputEsc)
	w, h := termbox.Size()
	g, err = newPlayfield(w, h)
	if err != nil {
		panic(err)
	}

	redraw()

mainloop:
	for {
		switch ev := termbox.PollEvent(); ev.Type {
		case termbox.EventKey:
			switch ev.Key {
			case termbox.KeyEsc:
				break mainloop
			case termbox.KeyArrowUp:
				g.cursorToTile(ev)
			case termbox.KeyArrowDown:
				g.cursorToTile(ev)
			case termbox.KeyArrowLeft:
				g.cursorToTile(ev)
			case termbox.KeyArrowRight:
				g.cursorToTile(ev)
			default:
				switch ev.Ch {
				case 'w':
					g.cursorToTile(ev)
				case 's':
					g.cursorToTile(ev)
				case 'a':
					g.cursorToTile(ev)
				case 'd':
					g.cursorToTile(ev)
				case '1':
					g.guessTile(ev.Ch)
				case '2':
					g.guessTile(ev.Ch)
				case '3':
					g.guessTile(ev.Ch)
				case '4':
					g.guessTile(ev.Ch)
				case '5':
					g.guessTile(ev.Ch)
				case '6':
					g.guessTile(ev.Ch)
				case '7':
					g.guessTile(ev.Ch)
				case '8':
					g.guessTile(ev.Ch)
				case '9':
					g.guessTile(ev.Ch)
				default:
					//DEBUG
					g.pending = ev
				}
			}
		case termbox.EventError:
			panic(ev.Err)
		}
		redraw()
	}
}

const xpadding = 3
const ypadding = 2

var g gridui

func redraw() {
	g.prefresh()

	g.retile()
	g.debugPrint()
	termbox.Flush()
}

type Position struct {
	x int
	y int
}

type Cursor struct {
	ordinal int
	row     int
	col     int
	Position
}

type tile struct {
	revealed bool
	guess    int
	Cursor
}

type gridui struct {
	cursor     Cursor
	cornerSE   Position
	solved     [][]int
	play       []tile
	pending    termbox.Event
	tileNW     Position
	tileSE     Position
	debugMsg   string
	debugGuess string
	cardinal   int
	sqr        int
	endgame    bool
}

// newPlayfield initializes and generate a new puzzle grid
func newPlayfield(w int, h int) (g gridui, err error) {
	g = gridui{cardinal: 81, sqr: 9}
	p := sudoku.NewPuzzle(g.cardinal)
	g.solved, err = p.Generate()
	if err != nil {
		return
	}
	g.endgame = false
	g.tileNW = Position{x: 0, y: 0}
	g.tileSE = Position{x: 24, y: 16}
	g.cursor = Cursor{row: 4, col: 4, ordinal: 40}
	g.cursor.x = 12
	g.cursor.y = 8
	ctr := Position{x: w / 2, y: h / 2}
	if ctr.y > g.cursor.y {
		yoffset := ctr.y - g.cursor.y
		xoffset := (yoffset / ypadding) * xpadding

		g.tileSE.x = g.tileSE.x + xoffset
		g.tileSE.y = g.tileSE.y + yoffset

		g.tileNW.x = xoffset
		g.tileNW.y = yoffset

		g.cursor.x = 12 + xoffset
		g.cursor.y = 8 + yoffset
	}

	g.cornerSE = Position{x: w, y: h}

	rand.Seed(time.Now().Unix())
	g.play = g.revealTiles()
	err = nil
	return
}

// retile traces tiles per refresh
func (g *gridui) retile() {
	if g.endgame {
		return
	}

	winCondition := true

	for i := 0; i < g.cardinal; i++ {
		t := g.cachedTile(i)
		ch, _ := utf8.DecodeRuneInString(t)
		g.displayTile(i, ch)

		if !g.tileSuccess(i) {
			winCondition = false
		}
	}
	if winCondition {
		g.endgame = true
		g.bannerSuccess()
	}
}

func (g *gridui) inbounds() bool {
	switch g.pending.Key {
	case termbox.KeyArrowUp:
		if g.cursor.y > g.tileNW.y {
			return true
		}
	case termbox.KeyArrowDown:
		if g.cursor.y < g.tileSE.y {
			return true
		}
	case termbox.KeyArrowLeft:
		if g.cursor.x > g.tileNW.x {
			return true
		}
	case termbox.KeyArrowRight:
		if g.cursor.x < g.tileSE.x {
			return true
		}
	default:
		switch g.pending.Ch {
		case 'w':
			if g.cursor.y > g.tileNW.y {
				return true
			}
		case 's':
			if g.cursor.y < g.tileSE.y {
				return true
			}
		case 'a':
			if g.cursor.x > g.tileNW.x {
				return true
			}
		case 'd':
			if g.cursor.x < g.tileSE.x {
				return true
			}
		}
	}
	return false
}

func (g *gridui) cursorToTile(arrow termbox.Event) {
	g.pending = arrow
	if g.endgame {
		return
	}
	if !g.inbounds() {
		g.debugMsg = ""
		return
	}

	ord := g.cursor.ordinal
	switch arrow.Key {
	case termbox.KeyArrowUp:
		ord = g.cursor.ordinal - g.sqr
	case termbox.KeyArrowDown:
		ord = g.cursor.ordinal + g.sqr
	case termbox.KeyArrowLeft:
		ord = g.cursor.ordinal - 1
	case termbox.KeyArrowRight:
		ord = g.cursor.ordinal + 1
	default:
		switch arrow.Ch {
		case 'w':
			ord = g.cursor.ordinal - g.sqr
		case 's':
			ord = g.cursor.ordinal + g.sqr
		case 'a':
			ord = g.cursor.ordinal - 1
		case 'd':
			ord = g.cursor.ordinal + 1
		}
	}

	p := g.play[ord]
	calc := Cursor{ordinal: ord}
	calc.row = p.row
	calc.col = p.col
	calc.x = p.x
	calc.y = p.y

	g.debugMsg = fmt.Sprintf("%v:%v", g.pending.Key, calc)
	g.cursor = calc
}

// guessTile inputs the guess as tile on cursor position
func (g *gridui) guessTile(ch rune) error {
	if g.endgame {
		return nil
	}
	i := g.cursor.ordinal

	if g.play[i].revealed {
		// Tile began as revealed
		return nil
	}

	val, err := strconv.Atoi(string(ch))
	if err != nil {
		return err
	}
	g.play[i].guess = val

	g.debugGuess = fmt.Sprintf("%v ord: %d, v: %d", g.cursor, i, val)
	return nil
}

// tile draws a single element of the 9x9 grid
func (g *gridui) displayTile(i int, ch rune) {
	const coldef = termbox.ColorDefault
	const colgrn = termbox.ColorGreen | termbox.ColorYellow
	bg := termbox.ColorGreen
	fg := termbox.ColorBlue

	row, col := g.itorc(i)
	px := g.play[i].x
	py := g.play[i].y
	if px == 0 && py == 0 {
		px, py = g.cursorXY(i)
		g.play[i].x = px
		g.play[i].y = py
		g.play[i].row = row
		g.play[i].col = col
		g.play[i].ordinal = i
	}

	if px == g.cursor.x && py == g.cursor.y {
		// Cursor tile
		bg = termbox.ColorWhite
		fg = termbox.ColorRed
	} else if ((row < 3 || row > 5) && (col < 3 || col > 5)) ||
		(row > 2 && row < 6 && col > 2 && col < 6) {
		// Checker board coloring
		bg = termbox.ColorBlue
		fg = termbox.ColorMagenta
	}

	termbox.SetCell(px, py, ch, fg, bg)

	// Grid lines for sub-grids
	if col == 3 || col == 6 {
		termbox.SetCell(px-1, py, '|', colgrn, coldef)
	}
	if row == 3 || row == 6 {
		termbox.SetCell(px, py-1, '-', colgrn, coldef)
	}
}

// calcXY maps ordinal to the console cursor X,Y
func (g *gridui) cursorXY(i int) (xt int, yt int) {

	quo, mod := g.itorc(i)

	xt = g.tileNW.x + (xpadding * mod)

	yt = g.tileNW.y + (ypadding * quo)

	return xt, yt
}

// revealTiles mixes board tiles as concealed or revealed
func (g *gridui) revealTiles() []tile {

	x := make([]tile, g.cardinal)

	for i := 0; i < g.cardinal; i++ {
		t := tile{revealed: true}

		// pseudo random (todo difficulty playfields)
		if i%2 == 0 {
			// Hide even tiles 2/3 of the time
			r := rand.Intn(100)
			if r >= 33 {
				t.revealed = false
			}
		}

		x[i] = t
	}
	return x
}

// cachedTile fetch tile value which can be in 3 states, solved/guess/none
func (g *gridui) cachedTile(i int) string {

	if g.play[i].revealed {
		r, c := g.itorc(i)
		return fmt.Sprintf("%v", g.solved[r][c])
	}

	if g.play[i].guess == 0 {
		return " "
	}
	return fmt.Sprintf("%v", g.play[i].guess)
}

// itorc maps ordinal to solved row, col
func (g *gridui) itorc(i int) (r int, c int) {
	// zero-index

	r = i / g.sqr
	c = i % g.sqr

	return
}

// tileSuccess checks tile at ordinal
func (g *gridui) tileSuccess(i int) bool {

	if !g.play[i].revealed {

		r, c := g.itorc(i)

		if g.solved[r][c] != g.play[i].guess {
			return false
		}
	}
	return true
}

// winCondition checks for solved grid
func (g *gridui) winCondition() bool {
	for i := 0; i < g.cardinal; i++ {

		if !g.play[i].revealed {

			r, c := g.itorc(i)

			if g.solved[r][c] != g.play[i].guess {
				return false
			}
		}
	}
	return true
}

// bannerSuccess displays completion message
func (g *gridui) bannerSuccess() {
	ctrx := g.cornerSE.x / 2
	ctry := g.cornerSE.y / 2
	ln0 := ctry - 3
	ln1 := ctry - 2
	ln2 := ctry - 1

	//  l0 := "W  W  W  I  NN  N"
	//  l1 := " W W W   I  N N N"
	//  l2 := "  W W    I  N  NN"
	l3 := "Press ESC to exit"
	tbprint(ctrx, ln0, termbox.ColorYellow, termbox.ColorGreen, " ")
	tbprint(ctrx+3, ln0, termbox.ColorYellow, termbox.ColorGreen, " ")
	tbprint(ctrx+6, ln0, termbox.ColorYellow, termbox.ColorGreen, " ")
	tbprint(ctrx+9, ln0, termbox.ColorYellow, termbox.ColorGreen, " ")
	tbprint(ctrx+12, ln0, termbox.ColorYellow, termbox.ColorGreen, "  ")
	tbprint(ctrx+16, ln0, termbox.ColorYellow, termbox.ColorGreen, " ")
	tbprint(ctrx+1, ln1, termbox.ColorYellow, termbox.ColorGreen, " ")
	tbprint(ctrx+3, ln1, termbox.ColorYellow, termbox.ColorGreen, " ")
	tbprint(ctrx+5, ln1, termbox.ColorYellow, termbox.ColorGreen, " ")
	tbprint(ctrx+9, ln1, termbox.ColorYellow, termbox.ColorGreen, " ")
	tbprint(ctrx+12, ln1, termbox.ColorYellow, termbox.ColorGreen, " ")
	tbprint(ctrx+14, ln1, termbox.ColorYellow, termbox.ColorGreen, " ")
	tbprint(ctrx+16, ln1, termbox.ColorYellow, termbox.ColorGreen, " ")
	tbprint(ctrx+2, ln2, termbox.ColorYellow, termbox.ColorGreen, " ")
	tbprint(ctrx+4, ln2, termbox.ColorYellow, termbox.ColorGreen, " ")
	tbprint(ctrx+9, ln2, termbox.ColorYellow, termbox.ColorGreen, " ")
	tbprint(ctrx+12, ln2, termbox.ColorYellow, termbox.ColorGreen, " ")
	tbprint(ctrx+15, ln2, termbox.ColorYellow, termbox.ColorGreen, "  ")
	tbprint(ctrx, ctry, termbox.ColorYellow, termbox.ColorDefault, l3)
}

// prefresh clears shared state variables to pre- refresh
func (g *gridui) prefresh() {
	g.pending = termbox.Event{}
	g.debugMsg = ""
	if !g.endgame {
		termbox.Clear(termbox.ColorDefault, termbox.ColorDefault)
	}
}

func (g *gridui) debugPrint() {
	if !*debug {
		return
	}

	// NW marker
	a := Position{x: 0, y: 0}
	tbprint(a.x, a.y,
		termbox.ColorYellow, termbox.ColorRed,
		fmt.Sprintf("%v", a))

	// SE marker
	z := Position{x: g.cornerSE.x - 1, y: g.cornerSE.y - 1}
	tbprint(z.x-6, z.y,
		termbox.ColorYellow, termbox.ColorRed,
		fmt.Sprintf("%v", z))
	// Mid-RT marker
	mr := Position{x: g.cornerSE.x, y: g.cornerSE.y / 2}
	tbprint(mr.x-7, mr.y,
		termbox.ColorYellow, termbox.ColorRed,
		fmt.Sprintf("%v", mr))
	// Mid-UP marker
	mu := Position{x: g.cornerSE.x / 2, y: 0}
	tbprint(mu.x, mu.y,
		termbox.ColorYellow, termbox.ColorRed,
		fmt.Sprintf("%v", mu))

	// Arrow keypress
	tbprint(g.cornerSE.x-28, 0,
		termbox.ColorMagenta, termbox.ColorGreen,
		g.debugMsg)

	// Guess tile
	tbprint(g.cornerSE.x-28, 1,
		termbox.ColorMagenta, termbox.ColorBlack,
		g.debugGuess)

	// Event
	txt := fmt.Sprintf("%v;%d,%c", g.cursor,
		g.pending.Key, g.pending.Ch)
	tbprint(g.cornerSE.x-28, g.cornerSE.y-1,
		termbox.ColorYellow, termbox.ColorDefault,
		txt)
}

func tbprint(x, y int, fg, bg termbox.Attribute, msg string) {
	for _, c := range msg {
		termbox.SetCell(x, y, c, fg, bg)
		x++
	}
}
