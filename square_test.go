package sudoku

import (
	"testing"
)

func TestWidthSquare(t *testing.T) {
	s := NewSquare(4)
	if s.Width() != 2 {
		t.Error(`Square Width error`)
	}
}

func TestNegativeCompleteSquare(t *testing.T) {
	s := NewSquare(4)
	buf := []byte(`{ "square": [[1,2],[3,9]] }`)
	err := s.Set(buf)
	if err != nil {
		t.Error(err)
	}

	if s.Complete() != false {
		t.Error(`Square Complete verification is incorrect, expected FALSE`)
	}
}

func TestGoodCompleteSquare(t *testing.T) {
	s := NewSquare(4)
	buf := []byte(`{ "square": [[1,2],[3,4]] }`)
	err := s.Set(buf)
	if err != nil {
		t.Error(err)
	}

	if s.Complete() != true {
		t.Error(`Square Complete verification is incorrect, expected TRUE`)
	}
}

func TestNegativeNineCompleteSquare(t *testing.T) {
	s := NewSquare(9)
	buf := []byte(`{ "square": [[9,2,3],[4,5,6],[7,8,9]] }`)
	err := s.Set(buf)
	if err != nil {
		t.Error(err)
	}

	if s.Complete() != false {
		t.Error(`Square Complete verification is incorrect, expected FALSE`)
	}
}

func TestGoodNineCompleteSquare(t *testing.T) {
	s := NewSquare(9)
	buf := []byte(`{ "square": [[1,2,3],[4,5,6],[7,8,9]] }`)
	err := s.Set(buf)
	if err != nil {
		t.Error(err)
	}

	if s.Complete() != true {
		t.Error(`Square Complete verification is incorrect, expected TRUE`)
	}
}
