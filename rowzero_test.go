package sudoku

import (
	"testing"
)

func TestRowzero(t *testing.T) {
	r := newRowzero(9, 1)
	if r.max != 9 {
		t.Error(`Rowzero max value is incorrect`)
	}
}

func TestTilesRowzero(t *testing.T) {
	r := newRowzero(9, 1)
	a := []int{1, 7, 9, 8, 5, 6, 4, 2, 3}
	for k, v := range r.Tiles() {
		if v != a[k] {
			t.Error(`Rowzero tiles is incorrect`)
		}
	}
}

func TestShiftRowzero(t *testing.T) {
	r := newRowzero(9, 1)
	rot1 := r.Shift(1)
	a1 := []int{7, 9, 8, 5, 6, 4, 2, 3, 1}
	for k, v := range rot1 {
		if v != a1[k] {
			t.Error(`Rowzero shift 1 is incorrect`)
		}
	}
	rot2 := r.Shift(2)
	a2 := []int{9, 8, 5, 6, 4, 2, 3, 1, 7}
	for k, v := range rot2 {
		if v != a2[k] {
			t.Error(`Rowzero shift 2 is incorrect`)
		}
	}
}
