# sudoku
Porting the sudoku clone to Golang. Termbox-go is used to display
 the puzzle inside the console.

![Image of sudoku console](https://shrmpy.github.io/twitch/images/full/sudoku.png)

## With Docker

1. Install [Docker](https://docker.com/)
2. Git clone the [repo](https://github.com/shrmpy/sudoku)
3. From the sudoku/cmd directory, build and run

```console
$ docker build -t sudoku .
$ docker run -it --rm sudoku
```

