package sudoku

import (
	"encoding/json"
	//	"fmt"
	"math"
)

type Square interface {
	Width() int
	Complete() bool
	Set([]byte) error
}

type SquareBase struct {
	max   int
	tiles []int
	table [][]int
}

func NewSquare(n int) Square {
	s := &SquareBase{}
	s.max = n
	s.tiles = make([]int, n)

	for i := 0; i < n; i++ {
		s.tiles[i] = i + 1
	}
	return s
}

func (s *SquareBase) Set(buf []byte) error {

	var jo struct {
		Table [][]int `json:"square"`
	}
	if err := json.Unmarshal(buf, &jo); err != nil {
		return err
	}

	s.table = jo.Table
	return nil
}

func (s *SquareBase) Width() int {
	return int(math.Sqrt(float64(s.max)))
}

func (s *SquareBase) Tiles() []int {
	return s.tiles
}
func (s *SquareBase) Table() [][]int {
	return s.table
}

func (s *SquareBase) Complete() bool {

	counts := make(map[int]int)
	for _, v := range s.tiles {
		// Zero the count totals
		counts[v] = 0
	}

	for _, row := range s.table {
		for _, tile := range row {
			// Count the tile occurance inside the box.
			if c, ok := counts[tile]; ok {
				counts[tile] = c + 1
			}
		}
	}

	for _, v := range counts {
		// Each tile must appear once, and only once.
		if v != 1 {
			return false
		}
	}

	return true
}
