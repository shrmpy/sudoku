package sudoku

import (
	"encoding/json"
	"errors"
)

type Puzzle interface {
	Generate() ([][]int, error)
}

type Grid9x9 struct {
	grid  [][]int
	max   int
	width int
}

func (p *Grid9x9) Generate() ([][]int, error) {
	p.randomizeGrid(0)
	x, err := p.solve()
	if err != nil {
		return [][]int{}, err
	}
	return x, nil
}

func NewPuzzle(total int) Puzzle {
	if total != 81 {
		panic("Total number of tiles currently supported: 81")
	}

	p := &Grid9x9{}
	p.max = 9
	p.width = 3
	p.grid = make([][]int, 9)
	return p
}

func (p *Grid9x9) randomizeGrid(seed int64) {

	r0 := newRowzero(p.max, seed)
	p.grid[0] = r0.Tiles()

	for i := 1; i < p.max; i++ {

		p.grid[i] = r0.Shift(i)
	}
}

func (p *Grid9x9) solve() ([][]int, error) {

	tmp, err := solve_first_subgrid(p.grid, p.max, p.width)
	if err != nil {
		return [][]int{}, err
	}

	tmp, err = solve_second_subgrid(tmp, p.max, p.width)
	if err != nil {
		return [][]int{}, err
	}

	tmp, err = solve_third_subgrid(tmp, p.max, p.width)
	if err != nil {
		return [][]int{}, err
	}

	p.grid = tmp
	return p.grid, nil
}

func solve_first_subgrid(gr [][]int, max int, sz int) ([][]int, error) {
	// starting with r0,
	// r1 needs to swap with r3 immediately
	// r2 needs to swap with r6 immediately

	tmp := permutation(gr, 1, sz)
	tmp = permutation(tmp, 2, 2*sz)

	solved := verifysub(tmp, 0*sz, sz, max)
	if !solved {
		return [][]int{}, errors.New("First solve sub-grid algo is failed")
	}
	return tmp, nil
}

func solve_second_subgrid(gr [][]int, max int, sz int) ([][]int, error) {
	// r5 needs to swap with r7

	tmp := permutation(gr, 5, 7)

	solved := verifysub(tmp, 1*sz, sz, max)
	if !solved {
		return [][]int{}, errors.New("Second solve sub-grid algo is failed")
	}
	return tmp, nil
}

func solve_third_subgrid(gr [][]int, max int, sz int) ([][]int, error) {
	// should be complete
	tmp := gr
	solved := verifysub(tmp, 2*sz, sz, max)
	if !solved {
		return [][]int{}, errors.New("Second solve sub-grid algo is failed")
	}
	return tmp, nil
}

// verifysub looks at each RxR square for the sub-grid
func verifysub(sub [][]int, start int, width int, max int) bool {

	for col := 0; col < max; col += width {

		s, err := getsqr(sub, width, col, start)
		if err != nil {
			panic(err)
		}
		if !s.Complete() {
			return false
		}
	}
	return true
}

// getsqr takes the values inside the box with col,row to col+size,row+size
func getsqr(sub [][]int, size int, col int, row int) (Square, error) {

	b := make([][]int, size)
	for i := 0; i < size; i++ {
		a := make([]int, size)
		b[i] = a
	}

	for n := 0; n < size; n++ {

		y := row + n

		for i := 0; i < size; i++ {

			x := col + i
			b[n][i] = sub[y][x]
		}
	}

	var jo struct {
		Table [][]int `json:"square"`
	}
	jo.Table = b
	buf, err := json.Marshal(jo)
	if err != nil {
		panic(err)
	}

	s := NewSquare(size)
	s.Set(buf)
	return s, nil
}

// permutation shuffling rows with goal of
// i>pivot are pool and i<pivot are "solved"
func permutation(gr [][]int, pivot int, slider int) [][]int {

	sz := len(gr[pivot])
	tmp := make([][]int, sz)
	a := make([]int, sz)
	z := make([]int, sz)

	copy(a, gr[pivot])
	copy(z, gr[slider])

	for i, row := range gr {
		switch i {
		case pivot:
			tmp[i] = z
		case slider:
			tmp[i] = a
		default:
			c := make([]int, sz)
			copy(c, row)
			tmp[i] = c
		}
	}

	return tmp
}
