package sudoku

import (
	"math"
	"math/rand"
	"time"
)

type rowzero struct {
	tiles []int
	max   int
}

func newRowzero(max int, seed int64) *rowzero {

	r := make([]int, 0, max)
	rz := &rowzero{}
	rz.max = max

	if seed == 0 {
		seed = time.Now().Unix()
	}
	rand.Seed(seed)

	for i := 0; i < max; i++ {

		v := rz.randval(r)
		r = append(r, v)
	}

	rz.tiles = r
	return rz
}

func (r *rowzero) Tiles() []int {
	t := make([]int, r.max)
	copy(t, r.tiles)
	return t
}

// Shift takes the first row r0 and shift by offset positions
func (r *rowzero) Shift(offset int) []int {
	//todo possible optimization,
	//copy slice from offset:end then append head 0:(offset-1)
	rot := make([]int, r.max)

	for k, _ := range r.tiles {

		i := k + offset

		if i > (r.max - 1) {
			n := math.Mod(float64(i), float64(r.max))
			i = int(n)
		}

		rot[k] = r.tiles[i]
	}

	return rot
}

// randval picks a random number that is not present in the list
func (r *rowzero) randval(state []int) int {

	exclusive := r.max + 1

	v := int(rand.Int31n(int32(exclusive)))
	if v == 0 {
		v++
	}

	for r.contains(v, state) {

		v = int(rand.Int31n(int32(exclusive)))
		if v == 0 {
			v++
		}
	}
	return v
}

func (r *rowzero) contains(target int, state []int) bool {
	for _, v := range state {
		if target == v {
			return true
		}
	}
	return false
}
