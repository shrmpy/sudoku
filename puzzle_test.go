package sudoku

import (
	"testing"
)

func TestGrid9x9(t *testing.T) {
	p := NewPuzzle(81)
	g := p.(*Grid9x9)
	if g.max != 9 {
		t.Error(`Puzzle max value is incorrect`)
	}
	if g.width != 3 {
		t.Error(`Puzzle width value is incorrect`)
	}
}

func TestRandomizeGrid9x9(t *testing.T) {
	p := NewPuzzle(81)
	g := p.(*Grid9x9)
	g.randomizeGrid(1)

	if len(g.grid[0]) != g.max {
		t.Error(`Puzzle r0 length is incorrect`)
	}
	if len(g.grid[8]) != g.max {
		t.Error(`Puzzle r8 length is incorrect`)
	}

	a0 := []int{1, 7, 9, 8, 5, 6, 4, 2, 3}
	a1 := []int{7, 9, 8, 5, 6, 4, 2, 3, 1}
	a2 := []int{9, 8, 5, 6, 4, 2, 3, 1, 7}
	a3 := []int{8, 5, 6, 4, 2, 3, 1, 7, 9}
	a4 := []int{5, 6, 4, 2, 3, 1, 7, 9, 8}
	a5 := []int{6, 4, 2, 3, 1, 7, 9, 8, 5}
	a6 := []int{4, 2, 3, 1, 7, 9, 8, 5, 6}
	a7 := []int{2, 3, 1, 7, 9, 8, 5, 6, 4}
	a8 := []int{3, 1, 7, 9, 8, 5, 6, 4, 2}

	for k, v := range g.grid[0] {
		if a0[k] != v {
			t.Error(`Puzzle r0 is incorrect`)
		}
	}
	for k, v := range g.grid[1] {
		if a1[k] != v {
			t.Error(`Puzzle r1 is incorrect`)
		}
	}
	for k, v := range g.grid[2] {
		if a2[k] != v {
			t.Error(`Puzzle r2 is incorrect`)
		}
	}
	for k, v := range g.grid[3] {
		if a3[k] != v {
			t.Error(`Puzzle r3 is incorrect`)
		}
	}
	for k, v := range g.grid[4] {
		if a4[k] != v {
			t.Error(`Puzzle r4 is incorrect`)
		}
	}
	for k, v := range g.grid[5] {
		if a5[k] != v {
			t.Error(`Puzzle r5 is incorrect`)
		}
	}
	for k, v := range g.grid[6] {
		if a6[k] != v {
			t.Error(`Puzzle r6 is incorrect`)
		}
	}
	for k, v := range g.grid[7] {
		if a7[k] != v {
			t.Error(`Puzzle r7 is incorrect`)
		}
	}
	for k, v := range g.grid[8] {
		if a8[k] != v {
			t.Error(`Puzzle r8 is incorrect`)
		}
	}
}

func TestPermutationGrid9x9(t *testing.T) {
	p := NewPuzzle(81)
	g := p.(*Grid9x9)
	g.randomizeGrid(1)

	t1 := permutation(g.grid, 1, g.width)
	a3 := []int{8, 5, 6, 4, 2, 3, 1, 7, 9}
	for k, v := range t1[1] {
		if a3[k] != v {
			t.Error(`Puzzle permutation 1 is incorrect`)
		}
	}

	t2 := permutation(g.grid, 2, 2*g.width)
	a6 := []int{4, 2, 3, 1, 7, 9, 8, 5, 6}
	for k, v := range t2[2] {
		if a6[k] != v {
			t.Error(`Puzzle permutation 2 is incorrect`)
		}
	}

	t5 := permutation(g.grid, 5, 7)
	a7 := []int{2, 3, 1, 7, 9, 8, 5, 6, 4}
	for k, v := range t5[5] {
		if a7[k] != v {
			t.Error(`Puzzle permutation 5 is incorrect`)
		}
	}
}

func TestVerifyGrid9x9(t *testing.T) {
	a0 := []int{1, 7, 9, 8, 5, 6, 4, 2, 3}
	a1 := []int{7, 9, 8, 5, 6, 4, 2, 3, 1}
	a2 := []int{9, 8, 5, 6, 4, 2, 3, 1, 7}
	a3 := []int{8, 5, 6, 4, 2, 3, 1, 7, 9}
	a4 := []int{5, 6, 4, 2, 3, 1, 7, 9, 8}
	a5 := []int{6, 4, 2, 3, 1, 7, 9, 8, 5}
	a6 := []int{4, 2, 3, 1, 7, 9, 8, 5, 6}
	a7 := []int{2, 3, 1, 7, 9, 8, 5, 6, 4}
	a8 := []int{3, 1, 7, 9, 8, 5, 6, 4, 2}

	g := make([][]int, 9)
	g[0] = a0
	g[1] = a1
	g[2] = a2
	g[3] = a3
	g[4] = a4
	g[5] = a5
	g[6] = a6
	g[7] = a7
	g[8] = a8
	expected := false
	solved := verifysub(g, 0, 3, 9)
	if solved != expected {
		t.Error(`Puzzle verify sub-grid is incorrect`)
	}

	expected = true
	g[0] = a0
	g[1] = a3
	g[2] = a6
	solved = verifysub(g, 0, 3, 9)
	if solved != expected {
		t.Error(`Puzzle verify sub-grid is incorrect`)
	}
}
